# Premium　「データ分析コース」の校正依頼

## 【背景】

新コースでデータ分析コースをリリースした

## 【依頼したいこと】

* データ分析コースの応用課題である、「Titanicの生存者予測」という課題の校正をお願いしたい。
このコースは、Kaggleのデータセットを利用してものの一部を日本語に翻訳したもので、これをAidemyの教材として利用したい。
* 現状、そのカーネルをそのまま利用すると、過去の教材と折り合いがつかなくなってしまう部分がある。例えば、Matplotlibを今まで学習してきているのに、このカーネルではいきなりSeabornを利用することになってしまいっている。これだと、学習内容をそのまま利用することができないので、過去の学習内容の範囲で、コースを完了できるようにしたい。
* また、教師あり学習に関しても、一部過去の課題でとりあつかっていない部分があり、これを既存の教師あり学習を利用したものだけを利用して応用課題に利用できるようにしたい。たとえば、ナイーブベイズ分析などの一部の分析手法に関しては、もともとのカリキュラムで扱っていない。既存のカリキュラムとちゃんと合致するような形で、コース作成をお願いしたい。
* また、これを作成したのが、データにそこまで詳しくない僕なので、データサイエンティストの人がみたときに、課題として問題がないか確認したい。

## 【懸念事項】

* Kaggleのデータの著作権。一応、カーネルに関しては全て内容をコピーしているわけではないので著作権的には問題はなさそうではある。データセットの取り扱いだけ気になっているので、Kaggleのサポートに問い合わせたら、以下のような答えが帰ってきた。

```
Hello Dai,

Thank you for your inquiry! All of our datasets include a license that details how the data can be used.

If you have any further questions about the usage of our datasets, you may get more answers by asking the dataset author. Click on the "Discussions" tab on a particular dataset's page and create a discussion there about your question.
```

* 現状メールでデータセットをKaggle側のものを使っていいか問い合わせ中
* 最悪使えない場合は、MITライセンスのtitanicのデータを利用しようかと考えている
* 現状、おそらくMITライセンスのデータなはずなので、問題ないと考えており、基本的にはこのカーネルのデータセットを利用するという流れでよさそう。

# タイタニック号のデータ予測

* 他の教材との折り合いを見るためにのファイルと、翻訳する前のKaggleのカーネル、また実際に翻訳したカーネルを保存してある。

`kernel.ipynb`: 実際のKaggleのカーネルからコピーしたもの。Google翻訳している。
`Titanic Analysis.ipynb`: 実際に日本語に河合が翻訳したもの、また一部初学者のために解説を追加したもの
`2010_ml_introduction`: 機械学習概論コース、こちらで教師あり学習とか教師なし学習に関しての解説をさらっとしている
`5020_classification`:教師あり学習の回帰について学べるコース
`4050_data_cleasing`: 前処理系のコース
`4010_Pandas`: Pandasを利用したデータハンドリングのコース
`4040_data_vidsualization`: Matplotlibを利用したグラフ化が学べるコース
`TitanicvAnalysis.ipynb` : カーネルを翻訳して、初学者でも理解できやすいように加筆したもの
`TitanicvAnalysis Excercise.ipynb` :TitanicAnalysis.ipynbのコード実行部分を決して、実際に課題としてとけるもの


